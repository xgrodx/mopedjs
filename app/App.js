require('../lib/mopedjs/moped');

var moped = new Moped();

moped.get('/', function( vars )
{
	moped.render( "index", 
						{
							'name'	: vars['name'],
							'title' : 'MOPED WEB FRAMEWORK'
						});
});

moped.get('/products', function( vars )
{
	moped.render( "index", 
						{
							'name'	: vars['name'] + ' prod',
							'title' : 'MOPED WEB FRAMEWORK'
						});
});

moped.get('/test', function(vars)
{
	moped.render("test");
});