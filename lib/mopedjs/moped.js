/**

	Moped node.js web framework built on top of vevarm web server.
	
	Author: Gustav Buchholtz (gustav.buchholtz@gmail.com), http://www.grod.se/
	
	* ----------------------------------------------------------------------------
	* "THE BEER-WARE LICENSE" (Revision 42):
	* Gustav Buchholtz wrote this file. As long as you retain this notice you
	* can do whatever you want with this stuff. If we meet some day, and you think
	* this stuff is worth it, you can buy me a beer in return.
	* ---------------------------------------------------------------------------- 
*/
var vevarm 	= require('../vevarm/vevarm');
var http 	= require('http');
var fs 		= require('fs');

var mimetypes = require('../../app/config/mimetypes');

require('./mvParser');

Moped = function()
{
	vevarm.start(this, 8000);
	
	this.defaultVars = {
							'moped_version' : '0.1',
							'assets_root' : './app/assets'
						};
	
	this.getMap = [ ];
	
	this.controllers = [];
	this.views = [];
	
	this.mapControllers();
	this.mapViews( './app/views/' );
	
	this.currentRes = null;
	
	this.parser = new MvParser();
}

Moped.prototype.constructor = Moped;

Moped.prototype.mapControllers = function()
{
	fs.readdir('./app/controllers/', function(err, files)
	{
		for (var i = 0; i < files.length; i++)
		{
			
		}
	});
}

Moped.prototype.mapViews = function( dir )
{
	var self = this;
	fs.readdir( dir, function(err, files)
	{
		for (var i = 0; i < files.length; i++)
		{
			var file = files[i];
			console.log(files[i]);
			if (file.substring( file.length - 3, file.length ) == ".mv")
			{
				self.views[ file.substring( 0, file.length - 3) ] = dir + file;
			}
			else // is directory
			{
				self.mapViews( dir + file + "/" );
			}
		}
	});
}

Moped.prototype.handleRequest = function (req, res)
{	
	this.currentRes = res;
	
	var url = req.url;
	var urlq = '';
	
	if (url.indexOf('?') > -1)
	{
		urlq = url.substring(url.indexOf('?')+1)
		url = url.substring(0, url.indexOf('?'));
	}
	
	if (url[url.length - 1] == '/')
		url = url.substring(0, url.length-1);
	
	if (url == '')
		url = '/';
	
	var argsParsed = {};
	var args = urlq.split('&');
	for (var i = 0; i < args.length; i++)
	{
		var arg = args[i];
		if (arg.indexOf('=') == -1)
			argsParsed[arg] = true;
		else
			argsParsed[ arg.split('=')[0] ] = arg.split('=')[1];
	}
	
	if (req.method == "GET")
	{
		if ( this.getMap[url] )
		{
			this.getMap[url](argsParsed);
		}
		else if( mimetypes[url.split('.').pop()] )
		{
			this.serveFile( req, res );
		}
		else //
		{
			this.render('404');
		}
	}
	else
	{
		this.render('404');
	}	
}

Moped.prototype.serveFile = function( req, res )
{
	var self = this;
	var filename = this.defaultVars["assets_root"] + req.url;
	var fileExt = filename.split('.').pop();
	
	if (  mimetypes[fileExt]  )
	{
		fs.readFile(filename, function(err, data)
		{
			if (err)
			{
				self.render('404');
			}
			else
			{
				res.writeHead( vevarm.OK, 'Content-Type : '+mimetypes[fileExt] );
				res.end(data);
			}
		});
	}
	else
	{
		this.render('404');
	}
}

Moped.prototype.get = function( url, callback )
{
	this.getMap[url] = callback;
}

Moped.prototype.render = function( view, vars )
{
	var self = this;
	var response = this.currentRes;
	var viewFile = this.views[view];
	
	vars = this.mergeObjects( this.defaultVars, vars );
	
	fs.readFile( viewFile, function(err, data)
	{
		response.writeHead( vevarm.OK, "" );
		response.end( self.parser.parse(data, vars), 'utf-8' );
	});
	
	this.currentRes = null;
}

Moped.prototype.mergeObjects = function(obj1, obj2)
{
	if (!obj1)
		obj1 = {};
	if (!obj2)
		obj2 = {};
		
	for (var p in obj2)
	{
		obj1[p] = obj2[p];
	}
	return obj1;
}
