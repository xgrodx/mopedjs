MvParser = function()
{
	
}

MvParser.prototype.constructor = MvParser;

MvParser.prototype.parse = function( data, vars )
{
	data = String(data);
	var variables = data.match( /\<\%.+?\%>/g );  // find all <% %> tags
	
	if (vars)
	{
		for (var p in vars)
		{
			eval("var " + p + " = '" + vars[p] + "';"); // create variable
		}
	}
	
	if (variables && variables.length > 0)
	{
		for (var i = 0; i < variables.length; i++)
		{
			term = this.trim(variables[i]);
			data = data.replace(variables[i],  eval(term)   );
		}
	}
	
	return data;
}

MvParser.prototype.trim = function( str )
{
	while (str.indexOf("<%") > -1)
		str = str.replace("<%", "");
	while (str.indexOf("%>") > -1)
		str = str.replace("%>", "");
	
	while (str.substring(0,1) == ' ') // leading whitespaces
		str = str.substring(1, str.length);
		
	while (str.substring(str.length-1, str.length) == ' ') // trailing whitespaces
		str = str.substring(0, str.length - 1);
	
	return str;
}