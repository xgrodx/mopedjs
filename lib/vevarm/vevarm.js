/**
	Vevarm minimal node.js based webserver
	
	Author: Gustav Buchholtz (gustav.buchholtz@gmail.com), http://www.grod.se/
	
	* ----------------------------------------------------------------------------
	* "THE BEER-WARE LICENSE" (Revision 42):
	* Gustav Buchholtz wrote this file. As long as you retain this notice you
	* can do whatever you want with this stuff. If we meet some day, and you think
	* this stuff is worth it, you can buy me a beer in return.
	* ---------------------------------------------------------------------------- 
*/

var http = require('http');
var vevarm = this;
vevarm.version = "0.1";
vevarm.app = null;

require('./response');
require('./request');

/* HTTP Status codes */
vevarm.OK = 200;

vevarm.handleRequest = function( req, res )
{
	var request = new Request( req );
	var response = new Response( res );
	
	// Pass this to the application
	if ( vevarm.app )
	{
		vevarm.app.handleRequest( req, res );
	}
}

vevarm.start = function( useApp, port )
{
	vevarm.app = useApp;
	var server = http.createServer( 
		function( req, res )
		{
			vevarm.handleRequest( req, res )
		});
	server.listen( port || 3000 );
}